-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 17, 2019 at 09:50 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Projet_BDW`
--

-- --------------------------------------------------------

--
-- Table structure for table `Adherent`
--

CREATE TABLE `Adherent` (
  `NumAdherent` int(7) DEFAULT NULL,
  `Nom` varchar(100) DEFAULT NULL,
  `Prenom` varchar(100) DEFAULT NULL,
  `Sexe` char(1) DEFAULT NULL,
  `DateNaissance` date DEFAULT NULL,
  `idAdresse` int(11) DEFAULT NULL,
  `Clubs` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Adresse`
--

CREATE TABLE `Adresse` (
  `idAdresse` int(11) NOT NULL,
  `NumRue` int(11) NOT NULL,
  `Rue` varchar(255) NOT NULL,
  `CodePostal` int(11) NOT NULL,
  `Ville` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Adresse`
--

INSERT INTO `Adresse` (`idAdresse`, `NumRue`, `Rue`, `CodePostal`, `Ville`) VALUES
(1, 3, 'Ninon Vallin', 69390, 'Lyon');

-- --------------------------------------------------------

--
-- Table structure for table `Course`
--

CREATE TABLE `Course` (
  `idCourse` int(11) DEFAULT NULL,
  `Nom` varchar(100) DEFAULT NULL,
  `idEpreuve` int(11) DEFAULT NULL,
  `AnneeCrea` int(11) DEFAULT NULL,
  `MoisEpreuve` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Edition`
--

CREATE TABLE `Edition` (
  `Annee` year(4) NOT NULL,
  `idCourse` int(11) NOT NULL,
  `idSQL` int(11) NOT NULL,
  `NbParticipants` int(11) DEFAULT NULL,
  `plan` varchar(100) DEFAULT NULL COMMENT 'stocke l''adresse d''une image dans le serveur',
  `AdresseDeDepart` varchar(100) DEFAULT NULL,
  `DatesInscriptions` int(11) DEFAULT NULL,
  `DatesDepotsCertificats` int(11) DEFAULT NULL,
  `DateRecuperationDossards` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Epreuve`
--

CREATE TABLE `Epreuve` (
  `NomEpreuve` varchar(30) DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `Denivele` int(11) DEFAULT NULL,
  `idEpreuve` int(11) NOT NULL,
  `Age` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Resultat`
--

CREATE TABLE `Resultat` (
  `Dossard` int(11) NOT NULL,
  `rang` int(11) DEFAULT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(100) DEFAULT NULL,
  `sexe` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SitesWebs`
--

CREATE TABLE `SitesWebs` (
  `idCourse` int(11) NOT NULL,
  `URLSiteWeb` varchar(100) DEFAULT NULL,
  `idEpreuve` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SQL`
--

CREATE TABLE `SQL` (
  `idSQL` int(11) NOT NULL,
  `Resultat` int(11) DEFAULT NULL,
  `TempsPassage` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Tarifs`
--

CREATE TABLE `Tarifs` (
  `Age` int(11) NOT NULL,
  `Prix` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TempsPassage`
--

CREATE TABLE `TempsPassage` (
  `dossard` int(11) NOT NULL,
  `km` int(11) NOT NULL,
  `temps` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `idUsers` int(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`idUsers`, `login`, `password`, `type`) VALUES
(1, 'yohann', 'test', 'root'),
(2, 'root', '123', 'root');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Adresse`
--
ALTER TABLE `Adresse`
  ADD PRIMARY KEY (`idAdresse`);

--
-- Indexes for table `Edition`
--
ALTER TABLE `Edition`
  ADD UNIQUE KEY `Edition_pk_2` (`idCourse`),
  ADD UNIQUE KEY `Edition_pk` (`Annee`),
  ADD UNIQUE KEY `Edition_pk_3` (`idSQL`);

--
-- Indexes for table `Epreuve`
--
ALTER TABLE `Epreuve`
  ADD PRIMARY KEY (`idEpreuve`),
  ADD KEY `Age` (`Age`);

--
-- Indexes for table `Resultat`
--
ALTER TABLE `Resultat`
  ADD PRIMARY KEY (`Dossard`);

--
-- Indexes for table `SitesWebs`
--
ALTER TABLE `SitesWebs`
  ADD PRIMARY KEY (`idCourse`),
  ADD UNIQUE KEY `SitesWebs_idEpreuve_uindex` (`idEpreuve`);

--
-- Indexes for table `SQL`
--
ALTER TABLE `SQL`
  ADD PRIMARY KEY (`idSQL`);

--
-- Indexes for table `Tarifs`
--
ALTER TABLE `Tarifs`
  ADD PRIMARY KEY (`Age`),
  ADD UNIQUE KEY `Tarifs_Age_uindex` (`Age`);

--
-- Indexes for table `TempsPassage`
--
ALTER TABLE `TempsPassage`
  ADD PRIMARY KEY (`dossard`),
  ADD UNIQUE KEY `TempsPassage_pk_2` (`km`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`idUsers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Adresse`
--
ALTER TABLE `Adresse`
  MODIFY `idAdresse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `idUsers` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
