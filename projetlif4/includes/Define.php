<?php
define('WEBROOT', dirname(__FILE__));
define('ROOT', dirname(WEBROOT));
define('BASE_URL', dirname(dirname($_SERVER['SCRIPT_NAME'])));
define('ESPACEPERSO', BASE_URL."/projetlif4/controller/espaceperso.php");

function redirectionAutentification($cheminController) {
    /*
     * indique où se diriger selon l'url en cas d'erreur :
     */

    if($_SERVER['SCRIPT_NAME'] == "/BDW1/projetlif4/controller/".$cheminController)
    { //Si on est dans le fichier lui même '$cheminController'
        echo $cheminController; // on redirige vers la même page en cas d'erreur
    }
    else if ($_SERVER['SCRIPT_NAME'] == "/BDW1/projetlif4/index.php")
    { // Sinon si on est dans le 'index.php', on se dirige vers $cheminController (celui désiré)
        echo "controller/".$cheminController;
    }
    else if($_SERVER['SCRIPT_NAME'] != "/BDW1/projetlif4/controller/".$cheminController) {
        // Sinon, si on est dans un autre et qu'on veut aller dans celui désiré
        echo "/BDW1/projetlif4/controller/".$cheminController;
    }
    else { // Sinon on se dirige à la racine (index.php) dans le pire des cas
        echo "/BDW1/projetlif4/index.php";
    }
}

