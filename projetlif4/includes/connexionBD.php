<?php
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'yohann');
define('DB_PASSWORD', '1234');
define('DB_NAME', 'baselif4');

function errorLogin($message = "erreur! Réessayez : <br>") {
    $message = (string)$message;
    echo $message;
    include ('../index.php');
}

function getHeaderSQLRequest($sql) {
    $db = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME."", DB_USERNAME, DB_PASSWORD);
    $select = $db->query($sql);
    $total_column = $select->columnCount();
    $column = array();
    for ($counter = 0; $counter < $total_column; $counter ++) {
        $meta = $select->getColumnMeta($counter);
        $column[] = $meta['name'];
    }
    $column2 = array();
    foreach ($column as $item => $value) {
        $column2[$value] = $value;
    }
    return $column2;
}

function traiterRequete($sql, $host=DB_SERVER, $username=DB_USERNAME, $passwd=DB_PASSWORD, $dbname=DB_NAME) {
    $connection = new mysqli($host, $username, $passwd, $dbname);
    // Check connection
    if($connection->connect_error){
        die("ERROR: Échec de la connexion. " . $connection->connect_error); // inutile, TODO: Faire un catch sur mysqli_connect()
    } else { //  Utilisation de la database :
        $resultat_requete = $connection->query($sql);
        if ($resultat_requete) // Si la requête réussi, on stocke la table dans un tableau :
        {
            $table = array();
//            echo $resultat_requete->num_rows; // nombre de lignes
            $cmpt = 1;
            $table[0] = getHeaderSQLRequest($sql);
            while ($row = $resultat_requete->fetch_assoc()) {
                $table[$cmpt] = $row;
                $cmpt++;
            }
            mysqli_close($connection);
            return $table;
        } else {
//            echo "La requête n'a pas aboutie<br>"; TODO: Faire une gestion d'erreure lorsque la requête est fausse
            echo "";
        }
        mysqli_close($connection);
        return array();
    }
}

function Array2Table($tabRequete) {
//    print_r($tabRequete);
    echo "<style>\n#tabRequete{
    width: 100%;
    border-collapse: collapse;
}
#tabRequete th, #tabRequete td {
  padding: 3px;
  border: 1px solid #fff;
  text-align: center;
}
#tabRequete th {
  background: #999690;
  color: white;
  font-weight: bold;
}
</style>\n";
    if (empty($tabRequete)) { // Si le tableau est vide, on affiche rien
        echo null;
    } else {
        echo "\n<table id='tabRequete'>";
        foreach ($tabRequete as $valueTuple => $tuple) {
            if ($valueTuple == 0) {
                echo "\n\t<thead>"; // Entête
                echo "\n\t\t<tr>\n";
                echo "\t\t\t<th>#</th>\n";
                foreach ($tuple as $column) {
                    echo "\t\t\t<th>" . $column . "</th>\n";
                }
                echo "\t\t</tr>";
                echo "\n\t</thead>";
            } else if ($valueTuple == 1) {
                echo "\n\t<tbody>"; // Début du body
                echo "\n\t\t<tr>\n";
                echo "\t\t\t<th>" . $valueTuple . "</th>\n";
                foreach ($tuple as $column) {
                    echo "\t\t\t<td>" . $column . "</td>\n";
                }
                echo "\t\t</tr>";
            } else {
                echo "\n\t\t<tr>\n";
                echo "\t\t\t<th>" . $valueTuple . "</th>\n";
                foreach ($tuple as $column) {
                    echo "\t\t\t<td>" . $column . "</td>\n";
                }
                echo "\t\t</tr>";
            }
        }
        echo "\n\t</tbody>"; // Fin du body
        echo "\n</table>\n";
    }
}

function getConnection($host=DB_SERVER, $username=DB_USERNAME, $passwd=DB_PASSWORD, $dbname=DB_NAME) {
    return new mysqli($host, $username, $passwd, $dbname);
}

function securiteValueSQL($value, $connection) {
    // TODO: $value doit être mmodifié en donnée/résultat
    if ($connection->connect_error) {
        echo "ERROR: Échec de la connexion. " . $connection->connect_error; // inutile, TODO: Faire un catch sur mysqli_connect()
        mysqli_close($connection);
    } else {
        // Sécurité : évite des injections SQL, html ou XSS
        if (ctype_digit($value))
            $value = intval($value);
        else {
            $value = mysqli_real_escape_string($connection, $value);
            $value = addcslashes($value, '%_');
        } // Fin sécurité
    }
    mysqli_close($connection);
    return $value;
}

function securiteValuesSQL($tabValue, $host=DB_SERVER, $username=DB_USERNAME, $passwd=DB_PASSWORD, $dbname=DB_NAME) {
    if (is_array($tabValue)) {
        foreach ($tabValue as $cle => $value) {
            $tabValue[$cle] = securiteValueSQL($value, getConnection($host, $username, $passwd, $dbname));
        }
        return $tabValue;
    }
    return array();
}

function executerRequete($sql, $host=DB_SERVER, $username=DB_USERNAME, $passwd=DB_PASSWORD, $dbname=DB_NAME)
{
    $connection = new mysqli($host, $username, $passwd, $dbname);
    // Check connection
    if ($connection->connect_error) {
        echo "ERROR: Échec de la connexion. " . $connection->connect_error; // inutile, TODO: Faire un catch sur mysqli_connect()
        mysqli_close($connection);
        return false;
    } else {
//        mysqli_query($connection, $sql);
        $connection->query($sql);
        mysqli_close($connection);
        return true;
    }
}

function ajouterAdresse($numRue, $nomRue, $Ville, $Pays, $CodePostal) {
    $tabAdresseDemandee = traiterRequete("
SELECT * 
FROM adresse 
WHERE adresse.NumRue = '".$numRue."'
AND adresse.Rue = '".$nomRue."'
AND adresse.Ville = '".$Ville."'
AND adresse.CodePostal = '".$CodePostal."'
AND adresse.Pays = '".$Pays."'
;
");

    if (sizeof($tabAdresseDemandee) == 1) // Si l'adresse n'existe pas, on l'ajoute
    {
        executerRequete("INSERT INTO `adresse` (`idAdresse`, `NumRue`, `Rue`, `Ville`, `Pays`, `CodePostal`)
VALUES (NULL, '".$numRue."', '".$nomRue."', '".$Ville."', '".$Pays."', '".$CodePostal."');");
        $idAdresse = traiterRequete("SELECT MAX(Adresse.idAdresse) FROM `adresse`;");
        return $idAdresse[1]['MAX(adresse.idAdresse)'];
    } else // Sinon, on utilise celle déjà existante
    {
        return $tabAdresseDemandee[1]['idAdresse'];
    }
}

function getDernierNumAdherent() {
    $result = traiterRequete("SELECT MAX(adherent.NumAdherent) FROM `adherent`;");
    print_r($result);
    return $result[1]['MAX(adherent.NumAdherent)'];
}

function generateDernierNumAdherent() {

    $lastNumAdherent = str_split(getDernierNumAdherent());
    $currentYear = str_split(date("Y"));
    $cmptYearCompare = 0;
    foreach ($currentYear as $item => $value) {
        if ($value == $lastNumAdherent[$item]) {
            $cmptYearCompare = $cmptYearCompare + 1;
        }
    }

    // Si le dernier adhérent n'est pas inscrit pour l'année courante,
    // alors il faut initialiser le premier adhérent de cette année
    if ($cmptYearCompare < 4)
    {
        $currentYear[4] = 0;
        $currentYear[5] = 0;
        $currentYear[6] = 0;
        return (int) implode('', $currentYear);
    }
    $lastNumAdherent = getDernierNumAdherent();
    $lastNumAdherent = $lastNumAdherent + 1;
    $lastNumAdherent = str_split($lastNumAdherent);

    $nextYear = date("Y") + 1;
    $nextYear = str_split($nextYear);

    if ($nextYear[3] == $lastNumAdherent[3]) // Si on est au max d'inscription :
    {
        return 0; // Le numéro d'adhérent est 0 si c'est plein
    } else // Il reste encore de la place
    {
        return getDernierNumAdherent() + 1; // on renvoie
    }

}

function ajouterAdherent($Nom, $Prenom, $Sexe, $DateNaissance, $idAdresse, $Clubs) {
    $tabAdherentDemandee = traiterRequete("
SELECT * 
FROM adherent
WHERE adherent.Nom = '" . $Nom . "'
AND adherent.Prenom = '" . $Prenom . "'
AND adherent.Sexe = '" . $Sexe . "'
AND adherent.DateNaissance = '" . $DateNaissance . "'
;");
    if (sizeof($tabAdherentDemandee) == 1) // Si l'adhérent n'existe pas, on l'ajoute
    {
        executerRequete("
INSERT INTO `adherent` (`NumAdherent`, `idEpreuves`, `Nom`, `Prenom`, `Sexe`, `DateNaissance`, `idAdresse`, `Clubs`)
VALUES ('".generateDernierNumAdherent()."', NULL, '".$Nom."', '".$Prenom."', '".$Sexe."', '".$DateNaissance."', '$idAdresse', '$Clubs');");
        return getDernierNumAdherent();
    } else // Sinon, on renvoie son numéro adhérant pour pouvoir le réutiliser
    {
        return $tabAdherentDemandee[1]['NumAdherent'];
    }
}

function ajouterUser($Login, $Password, $idAderent=null) {
    $tabUserDemande = traiterRequete("
SELECT * FROM `users`
WHERE users.login LIKE '". $Login ."'
");

    if ($idAderent == null) // Dans le cas où l'on souhaite ajouter un admin,
        // cela signifie qu'il n'y a pas d'id adhérent
    {
        if (sizeof($tabUserDemande) == 1) // Si le login n'existe pas, on peut l'ajouter
        {
            executerRequete("
INSERT INTO `users` (`login`, `password`, `type`, `idAdherent`)
VALUES ('".$Login."', '".$Password."', 'admin', NULL);");
            return $Login;
        } else {
            return null;
        }
    } else
    {
        if (sizeof($tabUserDemande) == 1) // Si le login n'existe pas, on peut l'ajouter
        {
            executerRequete("
INSERT INTO `users` (`login`, `password`, `type`, `idAdherent`)
VALUES ('".$Login."', '".$Password."', 'adherent', '".$idAderent."');");
            return $Login;
        } else {
            return null;
        }
    }
}

function renvoyerDonneesVersFormulaire($tabForm, $tabFormAllowed, $id = 'InscriptionAdherent') {
    echo "\n<script type='text/javascript'>\n";
    foreach ($tabForm as $numForm => $valueForm) {

        if (!$tabFormAllowed[$numForm]) // La valeur qui est fausse :
        {
            // Afficher le fond de la valeur en rouge
            echo "\ndocument.getElementById('". $id."').elements['".$numForm."'].style = 'background-color: red';\n";
        }
        if (!($numForm == "pPassword" or $numForm == "pPassword2")) {
            echo "document.getElementById('". $id."').elements['" . $numForm . "'].value = '". $valueForm . "';\n";
        }
    }
    echo "</script>";
}