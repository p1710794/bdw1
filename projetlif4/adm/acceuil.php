<?php

include_once ('../includes/connexionBD.php');

echo "<h1>ADMINISTRATEUR</h1>";

function espace($rang)
{
    $ch= '';
    for ($x=0;$x<$rang;$x++)
    {
        $ch.= '&nbsp;&nbsp;&nbsp;&nbsp;';
    }
    return $ch;
}

$tabCourse = traiterRequete("SELECT * FROM `course`;");
//Array2Table(traiterRequete("SELECT * FROM `course`;"));

function addToArrayArborescence($rang, $value) {
    return array('rang' => $rang, 'value' => $value);
}

function getEditionsFromCourse($idEdition) {
    return traiterRequete("
SELECT Editions, Annee, Epreuves
FROM course JOIN decline ON course.Editions = decline.idDecline JOIN edition ON decline.Edition = edition.Annee
WHERE Editions = ".$idEdition.";");
}

function getEpreuvesFromEdition($idEpreuve) {
    return traiterRequete("
SELECT * FROM `edition` JOIN comporte ON edition.Epreuves = comporte.idComporte JOIN epreuve ON comporte.Epreuve = epreuve.idEpreuve
WHERE Epreuves = ".$idEpreuve.";");
}

$tableToPrint = array();
$tableToPrint['name'] = 'Courses';
foreach ($tabCourse as $item => $value) {
    if ($item != 0) {
        $tableToPrint[$item] = addToArrayArborescence(0, $value['Nom']);
        if ($value['Editions'] != null) {
            $tabEdition = getEditionsFromCourse($value['Editions']);
            foreach ($tabEdition as $item2 => $value2) {
                if ($item2 != 0) {
                    $tableToPrint[$item][$item2] = addToArrayArborescence(1,$tabEdition[$item2]['Annee']." :");
                    if ($value2['Epreuves'] != null) {
                        $tabEpreuve = getEpreuvesFromEdition($value2['Epreuves']);
                        foreach ($tabEpreuve as $item3 => $value3) {
                            if ($item3 != 0) {
                                $tableToPrint[$item][$item2][$item3] = addToArrayArborescence(2, $value3['NomEpreuve']);
                            }
                        }
                    }
                }
            }
        }
    }
}


function TablePrint($tab) {
    $cmpt = 0;
    foreach ($tab as $item => $value) {
        $cmpt = $cmpt + 1;
        if (is_array($value)) {
            if ($value['rang'] == 0) {
                echo "<div id='Course'>";
                echo "<div id='nomCourse".$cmpt."' onclick='HiddenById(\"CourseBody\",$cmpt);'>".espace(0).$value['value']."</div>";
                echo "<div id='CourseBody".$cmpt."'>";
            }
            if ($value['rang'] == 1) {
                echo "<div id='Edition'>";
                echo "<div id='nomEdition' onclick='HiddenById(\"EditionBody\",$cmpt);'>".espace(1).$value['value']."</div>";
                echo "<div id='EditionBody".$cmpt."'>";
            }
            if ($value['rang'] == 2) {
                echo "<div id='Epreuve'>";
                echo "<div id='nomEpreuve".$cmpt."' onclick='HiddenById(\"EpreuveBody\",$cmpt);'>".espace(2).$value['value']."</div>";
                echo "<div id='EpreuveBody".$cmpt."'>";
            }

            TablePrint($value);

            if ($value['rang'] == 2) {
                echo "</div>";
            }
            if ($value['rang'] == 1) {
                echo "</div>";
            }
                if ($value['rang'] == 0) {
                echo "</div>";
                echo "</div>";
            }
        }

    }
}

echo "<script type='text/javascript'>
function f() {
  ShowById('CourseBody',3);
}
function g() {
  HiddenById('CourseBody',3);
}

function HiddenById(id, value) {
    if (document.getElementById(id.concat(value)) != null) {
        document.getElementById(id.concat(value)).style.display='none';
        document.getElementById('nomCourse3').onclick=f;
    } 
}
function ShowById(id, value) {
  document.getElementById(id.concat(value)).style.display='';
  document.getElementById('nomCourse3').onclick=g;
}
</script>";

//echo "document.getElementById('Course0').style.display=\"none\";";

function headTable($valueThead) {
    echo "<table id='Course'>";
    echo "<thead id='nomCourse'><tr><th colspan='4' style='text-align: left;'>".$valueThead."</th></tr></thead>";
    echo "<tbody><tr><td>---</td><td>";
}

function TablePrint2($tab) {
    foreach ($tab as $item => $value) {
        if (is_array($value)) {
            if ($value['rang'] == 0) {
                headTable(espace(0).$value['value']);
            }
            if ($value['rang'] == 1) {
                headTable(espace(1).$value['value']);
            }
            if ($value['rang'] == 2) {
//                echo "<div id='Epreuve'>";
                echo "<div id='nomEpreuve'>".espace(2).$value['value']."</div>";
            }

            TablePrint($value);

            if ($value['rang'] == 2) {
                echo "</div>";
            }
            if ($value['rang'] == 1) {
                echo "</tr></td><tbody>";
                echo "</table>";
            }
            if ($value['rang'] == 0) {
                echo "</tr></td><tbody>";
                echo "</table>";
            }
        }
    }
}
echo "<div id='Tableau'>";
TablePrint($tableToPrint);
echo "</div>";
//TablePrint2($tableToPrint);
//tableauDur();



function tableauDur() {
    echo "
<table style='background-color: #999690;'>
    <thead style='background-color: aqua;'>
        <tr>
            <th colspan='4' style='text-align: left;'>MadeInLyon</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>-----</td><td>2014</td>
        </tr>
        <tr>
            <td>-----</td>
            <td>
<table style='background-color: yellow;'>
    <thead><tr><th colspan='4' style='text-align: left;'>2013</th></tr></thead>
    <body></body>
        <tr><td>:::</td><td>Maraton</td></tr>
        <tr><td>:::</td><td>
<table style='background-color: red;'>
    <thead><tr><th colspan='4' style='text-align: left;'>Maraton</th></tr></thead>
</table>

</td></tr>
</table></td>

        </tr>
    </tbody>
</table>";
}




function afficheArborescence($tab) {
    echo "<table style='background-color: crimson;'><tbody>";
    foreach ($tab as $item => $value) {
        if ($tab[$item]['rang'] == 0) {
            echo "<tr><td>
<table style='background-color: #999690;'>
    <thead style='background-color: aqua;'>
        <tr>
            <th colspan='4' style='text-align: left;'>".$tab[$item]['value']."</th>
        </tr>
    </thead>
    <tbody>";
        }

        if ($tab[$item]['rang'] == 1) {
            echo "<tr><td>---</td><td>2014</td></tr>";
            echo "<tr><td>-----</td><td>".$tab[$item]['value'];
            echo "</td></tr>";
        }


        if ($tab[$item]['rang'] == 0) {
            echo "</tbody></table>
</td></tr>";
        }
    }

    // Fin plus grand tableau
    echo "</tbody></table>";
}


function afficheArbo($tab) {
    echo "<div id='Arbo'>";
    foreach ($tab as $item => $value) {
        if ($item != 0) {
            if ($tab[$item-1]['rang'] >= $tab[$item]['rang']) {
                // Ouvre
                if ($tab[$item]['rang'] == 0) {
                    echo "<div id='Courses'>".espace($tab[$item]['rang']).$tab[$item]['value']."</div>\n";
                }
            }
        }
        echo $value['value'];
    }
    echo "</div>";
}

//echo "<br><br>\n";

//afficheArborescence($tableToPrint);

//print_r($data);

function afficheArborescence2($tab) {
    echo "<div id='Arborescence'>";
    foreach ($tab as $item => $value) {
        if ($tab[$item]['rang'] == 0) {
            echo "<div id='Courses'>".espace($tab[$item]['rang']).$tab[$item]['value']."</div>\n";
        }
        else if ($tab[$item]['rang'] == 1) {
            echo "<div id='Editions'>".$tab[$item]['value']."</div>\n";
        }
        else if ($tab[$item]['rang'] == 2) {
            echo "<div id='AnneeEdition'>".$tab[$item]['value']."</div>\n";
        }
        else if ($tab[$item]['rang'] == 3) {
            echo "<div id='Epreuves'>".$tab[$item]['value']."</div>\n";
        }
        else if ($tab[$item]['rang'] == 4) {
            echo "<div id='Epreuve'>".$tab[$item]['value']."</div>\n";
        }
        else {
            echo espace($tab[$item]['rang']).$tab[$item]['value']."<br>\n";
        }
    }
    echo "</div>";
}

//afficheArborescence2($tableToPrint);

//afficheArbo($tableToPrint);
