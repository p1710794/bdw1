<?php session_start();?>
<?php
$_SESSION = array();
include_once ('../includes/connexionBD.php');
include_once ('../includes/Define.php');


/*
 *
 * ********************************Adhérent*******************************
 *
 * */
if (isset($_POST['pInscriptionAdherent'])
    and isset($_POST['pName'])
    and isset($_POST['pSurname'])
    and isset($_POST['pSexe'])
    and isset($_POST['pAge'])
    and isset($_POST['pNbRue'])
    and isset($_POST['pNomRue'])
    and isset($_POST['pVille'])
    and isset($_POST['pCodePostal'])
    and isset($_POST['pPays'])
    and isset($_POST['pLogin'])
    and isset($_POST['pPassword'])
    and isset($_POST['pPassword2'])
) {
    $error_request = false;

    // Création d'un tableau dédié à l'inscription
    $tabForm = array();
    $tabForm['pName']       = $_POST['pName'];
    $tabForm['pSurname']    = $_POST['pSurname'];
    $tabForm['pSexe']       = $_POST['pSexe'];
    $tabForm['pAge']        = $_POST['pAge'];
    $tabForm['pNbRue']      = $_POST['pNbRue'];
    $tabForm['pCodePostal'] = $_POST['pCodePostal'];
    $tabForm['pVille']      = $_POST['pVille'];
    $tabForm['pPays']       = $_POST['pPays'];
    $tabForm['pNomRue']     = $_POST['pNomRue'];
    $tabForm['pLogin']      = $_POST['pLogin'];
    $tabForm['pPassword']   = $_POST['pPassword'];
    $tabForm['pPassword2']  = $_POST['pPassword2'];

    // Création d'un tableau de booléen permettant de vérifier quels valeurs ne sont pas correctes
    $tabFormAllowed = $tabForm;
    foreach ($tabFormAllowed as $numForm => $valueForm) {
        $tabFormAllowed[$numForm] = true;
    }

    /*  Nom et Prénom : (doit être une chaîne de caractère fait de [a-Z]*/
    $tabFormAllowed['pName'] = ctype_alpha($tabForm['pName']);
    $tabFormAllowed['pSurname'] = ctype_alpha($tabForm['pSurname']);
    if ( !$tabFormAllowed['pName'] or !$tabFormAllowed['pSurname'] ) {
        echo "Le nom doit être fait de caractères seulement";
        $error_request = true;
    }
    $tabForm['pName']       = strtolower($tabForm['pName']);
    $tabForm['pSurname']    = strtoupper($tabForm['pSurname']);

    /*  Sexe peut prendre seulement la valeure 'M' ou 'F'*/
    $tabFormAllowed['pSexe'] = ($tabForm['pSexe'] == 'M' or $tabForm['pSexe'] == 'F');
    if (!$tabFormAllowed['pSexe']) {
        echo "Problème de genre";
        $error_request = true;
    }

    /*  Vérifie que l'âge entré est de bon format et existe
    TODO: faire un âge de minimum 10ans pour s'inscrire */
    if (!strptime($tabForm['pAge'], "%Y-%m-%d")) {
        echo "Mauvais format de date <br>";
        $tabFormAllowed['pAge'] = false;
        $error_request = true;
    }
    $date = explode("-", $tabForm['pAge']);
    if(!checkdate($date[1], $date[2], $date[0])) {
        echo "La date est fausse! <br>";
        $tabFormAllowed['pAge'] = false;
        $error_request = true;
    }

    /*  Vérifie que c'est un entier*/
    $tabFormAllowed['pNbRue'] = ctype_digit($tabForm['pNbRue']);
    if (!$tabFormAllowed['pNbRue']) {
        echo "Ce n'est pas un nombre!";
        $error_request = true;
    }

    /*  Vérifie que c'est un CodePostal*/
    $tabFormAllowed['pCodePostal'] = preg_match("#^[0-9]{5}$#", $tabForm['pCodePostal']);
    if (!$tabFormAllowed['pCodePostal']) {
        echo "Ce n'est pas sous la forme d'un code Postal <br>";
        $error_request = true;
    }

    /*  Vérifie que c'est une chaîne de caractère compris entre [a-Z]*/
    $tabFormAllowed['pVille'] = ctype_alpha($tabForm['pVille']);
    $tabFormAllowed['pPays'] = ctype_alpha($tabForm['pPays']);
    $tabFormAllowed['pNomRue'] = ctype_alpha($tabForm['pNomRue']);
    if (!($tabFormAllowed['pVille'] and $tabFormAllowed['pPays'] and $tabFormAllowed['pNomRue'])) {
        echo "La ville ou le pays doit être une chaîne de caractère!";
        $error_request = true;
    }


    /*  Vérification d'un mot de passe fort??
        Vérification de l'égalité des deux mots de passe*/
    $tabFormAllowed['pPassword'] = $tabFormAllowed['pPassword2'] = ($tabForm['pPassword'] == $tabForm['pPassword2']);
    if ( !($tabFormAllowed['pPassword']) ) {
        echo "Les deux mots de passe sont différents, veuillez réessayer";
        $error_request = true;
    }

    securiteValuesSQL($tabForm);

    if ($error_request) // Dans le cas où il y a une erreure concernant les données rentrées
    {
        include ('../controller/inscription.php');
        renvoyerDonneesVersFormulaire($tabForm, $tabFormAllowed);
    }
    else // On inclue la personne avec son login dans la table `User` et on affiche sa page
    {
        $idAdresse = ajouterAdresse($tabForm['pNbRue'], $tabForm['pNomRue'], $tabForm['pVille'], $tabForm['pPays'], $tabForm['pCodePostal']);
        $idAdherent = ajouterAdherent($tabForm['pSurname'], $tabForm['pName'], $tabForm['pSexe'], $tabForm['pAge'], $idAdresse, 'MadeInLyon');
        $login = ajouterUser($tabForm['pLogin'], $tabForm['pPassword'], $idAdherent);

        if ($login != null) {
            echo "<script type='text/javascript'>alert(\" Votre compte ".$login." a bien été ajouté!\")</script>\n";
            header ("Location: ../view/adherent.php");
        } else {
            echo "<script type='text/javascript'>alert(\" Ce compte existe déjà, choisissez-en un autre à ajouter!\")</script>\n";
            include ('../controller/inscription.php');
            renvoyerDonneesVersFormulaire($tabForm, $tabFormAllowed, 'InscriptionAdherent');
        }
        /* TODO: Inclure la personne SI ELLE N'EXISTE PAS ENCORE
            dans la BD puis rediriger vers sa page avec un message
            d'alert pour dire que l'inscription s'est bien déroulée*/
    }
}

/*
 *
 * ********************************Admin*******************************
 *
 * */

elseif (isset($_POST['pInscriptionAdmin'])
    and isset($_POST['pLogin'])
    and isset($_POST['pPassword'])
    and isset($_POST['pPassword2']))
{
    $tabForm = array();
    $tabForm['pLogin']      = $_POST['pLogin'];
    $tabForm['pPassword']   = $_POST['pPassword'];
    $tabForm['pPassword2']  = $_POST['pPassword2'];
    $error_request = false;

    // Création d'un tableau de booléen permettant de vérifier quels valeurs ne sont pas correctes
    $tabFormAllowed = $tabForm;
    foreach ($tabFormAllowed as $numForm => $valueForm) {
        $tabFormAllowed[$numForm] = true;
    }

    $tabFormAllowed['pPassword'] = $tabFormAllowed['pPassword2'] = ($tabForm['pPassword'] == $tabForm['pPassword2']);
    if ( !($tabFormAllowed['pPassword']) ) {
        echo "Les deux mots de passe sont différents, veuillez réessayer";
        $error_request = true;
    }

    if ($error_request) // Dans le cas où il y a une erreure concernant les données rentrées
    {
        include ('../controller/inscription.php');
        renvoyerDonneesVersFormulaire($tabForm, $tabFormAllowed, 'InscriptionAdmin');
        echo "\n<script type='text/javascript'>
        selView(2,document.getElementById(\"tabs\").querySelector(\"ul\").querySelectorAll(\"li\")[1]);
        </script>\n";
    } else {
        $login = ajouterUser($tabForm['pLogin'], $tabForm['pPassword'], null);
        if ($login != null) // Si un user a été ajouté
        {
            echo "<script type='text/javascript'>alert(\" Votre compte ".$login." a bien été ajouté!\")</script>\n";
            header ("Location: ../adm/acceuil.php");
        } else {
            include ('../controller/inscription.php');
            renvoyerDonneesVersFormulaire($tabForm, $tabFormAllowed, 'InscriptionAdmin');
            echo "
<script type='text/javascript'>
alert(\" Ce compte existe déjà, veuillez choisir un autre login!\");
selView(2,document.getElementById(\"tabs\").querySelector(\"ul\").querySelectorAll(\"li\")[1]);
</script>\n";
        }
    }

}

/*
 *
 * ********************************Autre*******************************
 *
 * */
else {
    header ("Location: ../index.php");
}