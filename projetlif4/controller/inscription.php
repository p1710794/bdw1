<?php
include_once('../includes/Define.php');
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>projetlif4</title>
    <link rel="stylesheet" type="text/css" href="../webroot/css/style.css">
</head>
<body>




    <div id="tabs">
        <ul>
            <li onClick="selView(1, this)">Adhérent</li>
            <li onClick="selView(2, this)">Administrateur</li>
        </ul>

    <div id="tabcontent">
        <div id="svgpic" class="tabpanel" style="display:inline">
            <form action="../model/VerfiInscription.php" method="post" id="InscriptionAdherent">
                <br>
                <h3>Inscription d'un adhérent</h3>

                <fieldset>
                    <legend>Identitée</legend>
                    <label for="Surname">
                        Nom :
                        <input type="text" name="pSurname" class="pSurname" placeholder="Nom" id="Surname" required>
                    </label><br>
                    <label for="Name">
                        Prenom :
                        <input type="text" name="pName" placeholder="Prenom" id="Name" required>
                    </label><br>
                    <label for="Genre">
                        Sexe :<br>
                        M<input type="radio" name="pSexe" value="M" id="GenreM" required checked>
                        F<input type="radio" name="pSexe" value="F" id="GenreF" required>
                    </label><br>
                    <label for="Age">
                        Date de Naissance :
                        <input type="date" name="pAge" id="Age">
                    </label>
                </fieldset>

                <fieldset>
                    <legend>Adresse</legend>
                    <label for="NbRue">
                        <input type="number" name="pNbRue" placeholder="Numéro de rue" id="NbRue" required>
                    </label>
                    rue
                    <label for="NomRue">
                        <input type="text" name="pNomRue" placeholder="Nom de rue" id="NomRue" required>
                    </label>
                    <label for="Ville">
                        <input type="text" name="pVille" placeholder="Ville" id="Ville" required>
                    </label>
                    <label for="CodePostal">
                        <input type="number" name="pCodePostal" placeholder="Code Postal" id="CodePostal" required>
                    </label>
                    <label for="Pays">
                        <input type="text" name="pPays" placeholder="Pays" value="France" id="Pays" required>
                    </label>
                </fieldset>

                <fieldset>
                    <legend>Activation du compte</legend>
                    <label for="Login">
                        <input type="text" name="pLogin" placeholder="Login" id="Login" required>
                    </label><br>
                    <label for="Password">
                        <input type="password" name="pPassword" placeholder="Password" required id="Password">
                    </label><br>
                    <label for="Password2">
                        <input type="password" name="pPassword2" placeholder="Password again" id="Password2" required>
                    </label>
                </fieldset>

                <br><input type="submit" name="pInscriptionAdherent" value="S'inscrire">
            </form>
        </div>


        <div id="source" class="tabpanel" style="display:none">
            <form action="../model/VerfiInscription.php" method="post" id="InscriptionAdmin">
                <br>
                <h3>Inscription d'un admin</h3>
                <fieldset>
                    <legend>Activation du compte</legend>
                    <label for="Login">
                        <input type="text" name="pLogin" placeholder="Login" required>
                    </label><br>
                    <label for="Password">
                        <input type="password" name="pPassword" placeholder="Password" required>
                    </label><br>
                    <label for="Password2">
                        <input type="password" name="pPassword2" placeholder="Password again" required>
                    </label>
                </fieldset>

                <br><input type="submit" name="pInscriptionAdmin" value="S'inscrire">
            </form>
        </div>
    </div>
    </div>


<!--    TODO: Mettre ce script au propre et dans un fichier .js-->
<script type="text/javascript">
    function selView(n, litag) {
        let svgview = "none";
        let codeview = "none";
        switch(n) {
            case 1:
                svgview = "inline";
                break;
            case 2:
                codeview = "inline";
                break;
            // add how many cases you need
            default:
                break;
        }

        document.getElementById("svgpic").style.display = svgview;
        document.getElementById("source").style.display = codeview;
        let tabs = document.getElementById("tabs");
        let ca = Array.prototype.slice.call(tabs.querySelectorAll("li"));
        ca.map(function(elem) {
            elem.style.background="#F0F0F0";
            elem.style.borderBottom="1px solid gray"
        });

        litag.style.borderBottom = "1px solid white";
        litag.style.background = "white";
    }

    function selInit() {
        let tabs = document.getElementById("tabs");
        let litag = tabs.querySelector("li");   // first li
        litag.style.borderBottom = "1px solid white";
        litag.style.background = "white";
    }
    window.onload = function() {
        selInit();
    }
</script>

</body>
</html>

