<?php session_start();?>
<?php
$_SESSION = array();
include('../includes/connexionBD.php');
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<title>projetlif4</title>
    <link rel="stylesheet" type="text/css" href="../webroot/css/style.css">
</head>
<body>
<?php
if (isset($_POST['pLogin']) AND isset($_POST['pPwd']))
    //Si les variables pLogin et pPwd existent, on cherche dans la basse de donnée :
{
    $username = stripcslashes($_POST['pLogin']); //supprime les antislashs
    $password = stripcslashes($_POST['pPwd']); //supprime les antislashs
    // On sélectionne l'identifiant et on retourne le tuple dans $tupleIdentifiant
    $tupleIdentifiant = traiterRequete("SELECT * FROM users WHERE login='$username'");
//    print_r($tupleIdentifiant);
    if (!empty($tupleIdentifiant)) // Si un tuple existe
    {
        if ($password == $tupleIdentifiant[1]['password'] AND $username == $tupleIdentifiant[1]['login']) {
            $_SESSION['identifiant'] = $tupleIdentifiant[1];
            if ($tupleIdentifiant[1]['type'] == 'adherent') // Si c'est un adhérent, page d'un adhérent
            {
                include ('../adh/acceuil.php');
            } else if ($tupleIdentifiant[1]['type'] == 'admin') // Sinon, si admin, page admin
            {
                include ('../adm/acceuil.php');
            } else {
                include ('../view/error.php');
            }
        } else {
            errorLogin("mot de passe incorrecte");
        }
    } else { // empty array signifie qu'il n'existe pas l'identifiant demandé
        errorLogin("login ou mot de passe incorrect/inconnu, voulez vous vous inscrire?");
    }
} else {
    errorLogin("Identifiant ou mot de passe manquant");
}
echo "
</body>
</html>";